package com.bbva.techu.backendapirestmongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendApirestMongodbApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendApirestMongodbApplication.class, args);
    }

}
