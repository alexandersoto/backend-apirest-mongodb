package com.bbva.techu.backendapirestmongodb.producto.infrastructure;

import com.bbva.techu.backendapirestmongodb.producto.application.ProductoService;
import com.bbva.techu.backendapirestmongodb.producto.domain.ProductoModel;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductoController {
    final ProductoService productoService;

    public ProductoController(ProductoService productoService) {
        this.productoService = productoService;
    }

    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.findAll();
    }

    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoId(@PathVariable String id) {
        return productoService.findById(id);
    }

    @PostMapping("/productos")
    public ProductoModel postProductos(@RequestBody ProductoModel newProducto) {
        productoService.save(newProducto);
        return newProducto;
    }

    @PutMapping("/productos")
    public void putProductos(@RequestBody ProductoModel productoToUpdate) {
        productoService.save(productoToUpdate);
    }

    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModel productoToDelete) {
        return productoService.delete(productoToDelete);
    }
}