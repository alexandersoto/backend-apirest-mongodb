package com.bbva.techu.backendapirestmongodb.producto.application;

import com.bbva.techu.backendapirestmongodb.producto.domain.ProductoModel;
import com.bbva.techu.backendapirestmongodb.producto.domain.ProductoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    final ProductoRepository productoRepository;

    public ProductoService(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    public List<ProductoModel> findAll() {
        return productoRepository.findAll();
    }

    public Optional<ProductoModel> findById(String id) {
        return productoRepository.findById(id);
    }

    public ProductoModel save(ProductoModel entity) {
        return productoRepository.save(entity);
    }

    public boolean delete(ProductoModel entity) {
        try {
            productoRepository.delete(entity);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}